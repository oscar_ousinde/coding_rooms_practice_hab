'use strict';
const prompt = require('prompt-sync')();
const parseArgs = require('minimist');

const argv = parseArgs(process.argv.slice(2));

const name = argv.name;

if (!name) {
  prompt('Hola');
  const answer = prompt('¿Qué tal estás?:').toLocaleLowerCase();
  if (answer === 'bien') {
    prompt('Me alegro!');
  } else if (answer === 'mal') {
    prompt('Es una pena...');
  } else {
    prompt('Lo siento, no te he entendido');
  }
} else {
  prompt(`Hola ${name}`);
  const answer = prompt('¿Qué tal estás?:').toLocaleLowerCase();
  if (answer === 'bien') {
    prompt('Me alegro!');
  } else if (answer === 'mal') {
    prompt('Es una pena...');
  } else {
    prompt('Lo siento, no te he entendido');
  }
}
