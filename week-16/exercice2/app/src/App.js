import './App.css';
import { useState } from 'react';

const lista = [
  {
    id: 1,
    task: 'Ir a la compra',
    done: false,
  },
  {
    id: 2,
    task: 'Hacer ejercicios de react',
    done: true,
  },
];

function App() {
  const [list, setList] = useState(lista);

  return (
    <main>
      <TaskList list={list} setList={setList} />
    </main>
  );
}

const TaskList = ({ list, setList }) => {
  setList(list);

  return (
    <ul>
      {list.map((task) => (
        <li
          key={task.id}
          style={{ textDecoration: task.done && 'line-through' }}
        >
          {task.task}
        </li>
      ))}
    </ul>
  );
};

export default App;
