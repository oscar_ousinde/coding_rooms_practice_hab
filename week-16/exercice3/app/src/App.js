import { useState } from 'react';
import './App.css';

const lista = [
  {
    id: 1,
    task: 'Ir a la compra',
    done: false,
  },
  {
    id: 2,
    task: 'Hacer ejercicios de react',
    done: true,
  },
];

function App() {
  const [list, setList] = useState(lista);

  return (
    <main>
      <NewTask list={list} setList={setList} />
      <TaskList list={list} setList={setList} />
    </main>
  );
}

const TaskList = ({ list, setList }) => {
  const [check, setCheck] = useState(true);

  const manageClick = (e) => {
    setCheck(!check);

    const checkNum = e.target.id - 1;

    list[checkNum].done = check;
    setList(list);
  };
  return (
    <ul>
      {list.map((task) => (
        <li
          key={task.id}
          style={{ textDecoration: task.done && 'line-through' }}
        >
          {task.task}
          <input type='checkbox' id={task.id} onClick={manageClick} />
        </li>
      ))}
    </ul>
  );
};

const NewTask = ({ list, setList }) => {
  //Función evento onSubmit
  const addTask = (event) => {
    event.preventDefault();

    const newTaskInput = event.target.children.inputText.value;

    const newTaskStructure = {
      id: list.length + 1,
      task: newTaskInput,
      done: false,
    };

    const newList = [...list, newTaskStructure];
    setList(newList);

    event.target.children.inputText.value = '';
  };

  return (
    <form onSubmit={addTask}>
      <input
        name='inputText'
        type='text'
        maxLength='100'
        placeholder='New task...'
      ></input>
      <button>Add</button>
    </form>
  );
};

export default App;
