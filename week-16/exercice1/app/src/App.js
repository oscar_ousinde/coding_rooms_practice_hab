import './App.css';
import users from './users.json';
import legalAge from './icons/18.png';

function App() {
  return (
    <div className='App'>
      <header>
        <h1>People DB</h1>
      </header>
      <main>
        <UserList />
      </main>
      <footer>
        <p>HAB exercice 1 | week-16 | Oscar Ousinde</p>
      </footer>
    </div>
  );
}

const UserList = () => {
  return (
    <section className='user'>
      <ul>
        {users.map((info) => (
          <User
            key={info.login.uuid}
            userPicture={info.picture}
            userName={info.name}
            userLocation={info.location}
            date={info.dob.date}
          />
        ))}
      </ul>
    </section>
  );
};

const User = ({ userPicture, userName, userLocation, date }) => {
  const dateUser = new Date(date).getFullYear();

  const dateNow = new Date().getFullYear();

  const years = dateNow - dateUser;

  return (
    <li>
      <header className='headerName'>
        <UserName userName={userName} />
        <OverLegalAge years={years} />
      </header>
      <div>
        <UserPicture userPicture={userPicture} />
        <UserLocation userLocation={userLocation} />
      </div>
    </li>
  );
};

const UserPicture = ({ userPicture }) => {
  return <img src={userPicture.large} alt='user pic' className='userPicture' />;
};
const UserName = ({ userName }) => {
  return (
    <div className='completeName'>
      <h2>{userName.title}</h2>
      <h2>{userName.first}</h2>
      <h2>{userName.last}</h2>
    </div>
  );
};

const UserLocation = ({ userLocation }) => {
  return (
    <ul className='locationList'>
      <li>
        Street: {userLocation.street.name}, {userLocation.street.number}
      </li>
      <li>City: {userLocation.city}</li>
      <li>Postcode: {userLocation.postcode}</li>
      <li>State: {userLocation.state}</li>
      <li>Country: {userLocation.country}</li>
    </ul>
  );
};

const OverLegalAge = ({ years }) => {
  return (
    years >= 18 && <img className='over18' src={legalAge} alt='legal age' />
  );
};
export default App;
