Vamos a seguir construyendo sobre el ejercicio de la semana pasada, recuerdas esa lista de tareas que construímos paso a paso usando React? Pues lo primero que tienes que hacer es copiar el estado final del ejercicio 3 de la pasada semana al editor de la derecha para continuar trabajando en él.

Ahora mismo la aplicación funciona correctamente, puedes añadir tareas y marcarlas como acabadas pero hay un problema… si recargas la página toda la lista de tareas se pierde porque React no está guardando eses datos para que persistan, pronto aprenderás a guardar datos en una base de datos através de una API pero de momento vamos a hacer algo más sencillo: usar el localStorage.

Haz que el estado del componente principal App se guarde en el localStorage (busca información de como funciona localStorage en la documentación de DOM o en la MDN) de forma que aunque recarges la página el estado persista.

Para hacer esto debes hacer que:

cuando se carge la aplicación mire si hay un estado guardado en el localStorage y use ese estado como inicial.

cada vez que el estado se modifique porque se añade o modifica un elemento debe guardar el estado en el localStorage. Para hacer esto debes usar useEffect.

Recuerda que en e localStorage solo se pueden guardar cadenas de texto, pero el estado donde se guardan las tareas es un Array de JavaScript, por lo tanto tendrás que convertirlo a texto para guardarlo y después de leerlo convertir el texto guardado en un Array.
