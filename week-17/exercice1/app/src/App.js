import { useEffect, useState } from 'react';

import List from './components/List';

function App() {
  //variable con las tasks del LS
  const tasksFromLS = JSON.parse(localStorage.getItem('tasksFromLS'));

  const [tasks, setTasks] = useState(tasksFromLS ? tasksFromLS : []);

  //Actualización del LS al actualzar el state
  useEffect(() => {
    localStorage.setItem('tasksFromLS', JSON.stringify(tasks));
  }, [tasks]);

  return (
    <div className='app'>
      <List tasks={tasks} setTasks={setTasks} />
    </div>
  );
}

export default App;
