import { useState, useEffect } from 'react';
import { getPostsRequest } from '../services/getPostsRequest';

const usePosts = () => {
  const [listPosts, setListPosts] = useState([]);

  //   getPostsRequest();
  useEffect(() => {
    getPostsRequest(setListPosts);
  }, [listPosts]);

  return listPosts;
};

export { usePosts };
