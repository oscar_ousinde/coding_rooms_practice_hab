const getPosts_URL = 'https://jsonplaceholder.typicode.com/posts';

const getPostsRequest = async (setListPosts) => {
  const res = await fetch(getPosts_URL);
  if (!res.ok) {
    throw new Error('Not fetched!');
  } else {
    const data = await res.json();
    setListPosts(data);
  }
};

export { getPostsRequest };
