import { useState } from 'react';

const getPosts_URL = 'https://jsonplaceholder.typicode.com/posts';

const Form = ({ listPosts, setListPosts }) => {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [userId, setUserId] = useState(1);

  const handleSubmit = async (e) => {
    e.preventDefault();

    function uuidv4() {
      return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
          c ^
          (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
      );
    }

    const newPost = {
      userId,
      id: uuidv4(),
      title,
      body,
    };

    await fetch(getPosts_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(newPost),
    });

    setListPosts([newPost, ...listPosts]);

    setTitle('');
    setBody('');
    setUserId('');
  };

  return (
    <section>
      <header>
        <h1>Crea un nuevo post</h1>
      </header>
      <form onSubmit={handleSubmit}>
        <label>
          Title:
          <input
            type='text'
            required
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          ></input>
        </label>
        <label>
          Body:
          <textarea
            required
            value={body}
            onChange={(e) => setBody(e.target.value)}
          ></textarea>
        </label>
        <label>
          User Id:
          <input
            type='number'
            required
            min='1'
            value={userId}
            onChange={(e) => setUserId(e.target.value)}
          ></input>
        </label>
        <button>Send</button>
      </form>
    </section>
  );
};

export { Form };
