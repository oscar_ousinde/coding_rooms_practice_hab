const Posts = ({ listPosts }) => {
  return (
    <>
      <section>
        <ul>
          {listPosts.map((item) => {
            return (
              <li key={item.id}>
                <header>{item.title}</header>
                <p>{item.body}</p>
                <p>Author: {item.userId}</p>
              </li>
            );
          })}
        </ul>
      </section>
    </>
  );
};

export { Posts };
