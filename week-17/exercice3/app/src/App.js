import { useState, useEffect } from 'react';
import { getPostsRequest } from './services/getPostsRequest';

import './App.css';
import { Form } from './components/Form';
import { Posts } from './components/Posts';

function App() {
  const [listPosts, setListPosts] = useState([]);

  //getPostsRequest();
  useEffect(() => {
    getPostsRequest(setListPosts);
  }, []);

  return (
    <main>
      <Form listPosts={listPosts} setListPosts={setListPosts} />
      <Posts listPosts={listPosts} />
    </main>
  );
}

export default App;
