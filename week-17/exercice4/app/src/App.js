import './App.css';

import React from 'react';

import { Header } from './components/Header';
import { Content } from './components/Content';
import { useState } from 'react';

export const MyContext = React.createContext();

function App() {
  const [theme, setTheme] = useState('light');

  return (
    <MyContext.Provider value={{ theme, setTheme }}>
      <Header />
      <Content />
    </MyContext.Provider>
  );
}

export default App;
