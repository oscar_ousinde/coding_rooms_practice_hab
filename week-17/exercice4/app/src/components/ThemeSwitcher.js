import React, { useState } from 'react';
import { MyContext } from '../App';
import './ThemeSwitcher.css';

const ThemeSwitcher = () => {
  const context = React.useContext(MyContext);
  const { theme, setTheme } = context;

  const [icon, setIcon] = useState('🌑');

  const handleButton = () => {
    if (icon === '☀️') setIcon('🌑');
    if (icon === '🌑') setIcon('☀️');
    if (theme === 'light') setTheme('dark');
    if (theme === 'dark') setTheme('light');
  };

  return <button onClick={handleButton}>{icon}</button>;
};

export { ThemeSwitcher };
