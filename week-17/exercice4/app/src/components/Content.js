import { MyContext } from '../App';
import './Content.css';
import React from 'react';

const Content = () => {
  const context = React.useContext(MyContext);
  const { theme } = context;
  console.log(theme);
  return (
    <section className={theme}>
      <h2>Welcome to my app</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac velit
        malesuada, pharetra risus commodo, tristique dolor. Aenean efficitur
        lacus nisl, eget fermentum libero posuere id. Nulla facilisi. Integer
        vitae finibus nulla, non porta ante. Praesent metus urna, pulvinar ut
        pellentesque gravida, gravida in lorem. Phasellus id neque vel nibh
        consequat euismod. Nulla sed erat ligula. Vivamus laoreet, metus sed
        vulputate maximus, turpis nunc aliquet orci, non porttitor libero dui a
        nisl. Donec nec imperdiet neque, sed viverra urna. Morbi quam ante,
        aliquam nec nulla non, rhoncus gravida augue. Etiam in porta velit. Sed
        volutpat pharetra justo at facilisis. Fusce id justo tristique,
        elementum mi ac, hendrerit arcu. Sed convallis dolor ac dolor rhoncus,
        at mattis nisi ultricies. Vivamus ut felis eu velit finibus lobortis.
        Aenean nec purus rutrum, pellentesque lectus at, facilisis tellus.
        Praesent sed nulla tempor, imperdiet metus a, gravida risus. Nulla
        lectus massa, sodales eu fermentum ac, malesuada quis quam. Donec
        finibus tempus dapibus. In fringilla nibh nec turpis euismod egestas.
        Phasellus eu mattis ex. Curabitur euismod lobortis sapien semper
        condimentum.
      </p>
    </section>
  );
};

export { Content };
