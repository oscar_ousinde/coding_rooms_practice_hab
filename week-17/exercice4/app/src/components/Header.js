import { ThemeSwitcher } from './ThemeSwitcher';
import { Title } from './Title';
import './Header.css';

const Header = () => {
  return (
    <header>
      <Title />
      <ThemeSwitcher />
    </header>
  );
};

export { Header };
