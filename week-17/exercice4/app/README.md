Crea un Contexto y un Provider en la aplicación de React que gestione la preferencia visual del usuario que determina si la app se muestra en modo oscuro o claro.

La estructura de componentes de la aplicación debe ser similar a esta:

App

Header

Title

ThemeSwitcher

Content

El componente ThemeSwitcher debe contener un botón que permita cambiar el tema de la app a modo oscuro y modo claro.

El componente Content debe cambiar su estilo visual acorde a la selección de tema.

Para ello tienes no puedes pasar estados ni funciones vía props, debes usar estas características de React:

createContext

Context Provider

useContext

El resultado debe ser similar a este:

https://context-example-berto.vercel.app/
