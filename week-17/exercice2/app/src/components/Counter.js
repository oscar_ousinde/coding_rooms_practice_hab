import { useCounter } from '../Hooks/useCounter';

const Counter = () => {
  const utility = useCounter();

  const {
    value,
    setValue,
    startNumber,
    setStartNumber,
    handleSum,
    handleRest,
    handleStartNumber,
  } = utility;

  return (
    <section>
      <h1>El contador tiene un valor de {value}</h1>
      <form className='inputForm' onSubmit={(e) => e.preventDefault()}>
        <input
          type='number'
          value={startNumber}
          min='0'
          onChange={(e) => setStartNumber(e.target.value)}
        ></input>
        <button onClick={handleStartNumber}>Set counter</button>
      </form>
      <form className='sumRestForm' onSubmit={(e) => e.preventDefault()}>
        <button onClick={handleSum}>+1</button>
        <button onClick={handleRest}>-1</button>
      </form>
      <button onClick={() => setValue(0)}>RESET</button>
    </section>
  );
};

export { Counter };
