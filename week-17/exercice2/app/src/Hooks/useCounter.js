import { useState, useEffect } from 'react';

const useCounter = () => {
  const [value, setValue] = useState(0);
  const [startNumber, setStartNumber] = useState(0);

  useEffect(() => {
    if (value < 0) {
      alert('El contador no admite números negativos');
      setValue(0);
    }
  }, [value]);

  const handleSum = () => {
    setValue(Number(value) + 1);
  };

  const handleRest = () => {
    setValue(Number(value) - 1);
  };

  const handleStartNumber = () => {
    setValue(startNumber);
    setStartNumber(0);
  };

  return {
    value,
    setValue,
    startNumber,
    setStartNumber,
    handleSum,
    handleRest,
    handleStartNumber,
  };
};

export { useCounter };
