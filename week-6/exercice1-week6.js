//DEBE DE IMPRIMIR
// El mejor equipo es Toros Negros con un total de 28 puntos
// El peor equipo es Ciervos Celestes con un total de 10 puntos

'use strict';

// puntuaciones primera ronda
const firstRound = [
  { team: 'Toros Negros', scores: [1, 3, 4, 2, 10, 8] },
  { team: 'Águilas Plateadas', scores: [5, 8, 3, 2, 5, 3] },
  { team: 'Leones Carmesí', scores: [5, 4, 3, 1, 2, 3, 4] },
  { team: 'Rosas Azules', scores: [2, 1, 3, 1, 4, 3, 4] },
  { team: 'Mantis Verdes', scores: [1, 4, 5, 1, 3] },
  { team: 'Ciervos Celestes', scores: [3, 5, 1, 1] },
  { team: 'Pavos Reales Coral', scores: [2, 3, 2, 1, 4, 3] },
  { team: 'Orcas Moradas', scores: [2, 3, 3, 4] },
];

//Declaraciń de un nuevo array.
const arrayTotalScores = [];

//Con esto recorro el array firstRound y "pusheo" dentro la suma de la puntuación de cada equipo.
//Para hacer la suma se utiliza el método .reduce() retornando el acumuladr (acc).
//Haciendo un console.log(arrayTotalScores) se ve un array con la suma del total de puntos de cada equipo
for (const score of firstRound) {
  arrayTotalScores.push(
    score.scores.reduce((acc, points) => {
      return (acc += points);
    }, 0)
  );
}

//Aquí declaro dos variables para saber cuantos puntos corresponden al mejor y al peor equipo
let bestTeam = 0;
let worstTeam = 0;

//Con este bucle (un poco enrevesado) recorro el array arrayTotalScores y asigno a cada variable bestTeam o worstTeam la mayor y menor puntuación respectivamente
//La lógica de este bucle:
//Las variables inician en cero. Al recorrer el array cen cada ciclo con arrayTotalScores[i] se checkea si cada elemento del array es superior
//a la variable bestTeam. Si ese elemento del array es superior se asigna bestTeam = arrayTotalScores[i] y el worstTeam = bestTeam.
//Esto último es necesario porque en el else if hago lo mismo pero para obtener la menor puntuación del array y asignarla a worstTeam.
//Y funciona!! En resumen: en cada ciclo se compara el valor del elemento del array arrayTotalScores con el valor de las variables bestTeam y worstTeam.
//Para el bestTeam necesitamos la mayor puntuación y para el worstTeam la menor.
for (let i = 0; i <= arrayTotalScores.length; i++) {
  if (arrayTotalScores[i] > bestTeam) {
    bestTeam = arrayTotalScores[i];
    worstTeam = bestTeam;
  } else if (arrayTotalScores[i] < worstTeam) {
    worstTeam = arrayTotalScores[i];
  }
}

//Ahora declaro otras dos variables para extraer en cada una el nombre del equipo mejor y peor.
//Aquí está la magia pues el indexOf nos da el índice del array arrayTotalScores donde están la mayor y la menor puntuación
//al usar las variables bestTeam y worstTeam para acceder a el. Si esta lógica la englobamos en firstRound[].team, nos da
//los nombres de los equipos con mayor y menor puntuación en cada variable const mejor y const peor.
const mejor = firstRound[arrayTotalScores.indexOf(bestTeam)].team;
const peor = firstRound[arrayTotalScores.indexOf(worstTeam)].team;

//Después solo queda hacer el console.log() utilizando los template strings
console.log(`El mejor equipo es ${mejor} con un total de ${bestTeam} puntos`);
console.log(`El peor equipo es ${peor} con un total de ${worstTeam} puntos`);

//ESTA ES OTRA FORMA DE HACER LO MISMO PERO CON UNA FUNCIÓN.
//EN FUNCIÓN
const results = (result) => {
  //Creo array para acumular los puntos totales de cada equipo
  const arrayTotalScores = [];

  //Recorro array (firstRound) pasado como argumento y "pusheo" con un .reduce al array .arrayTotalScores
  for (const score of result) {
    arrayTotalScores.push(
      score.scores.reduce((acc, points) => {
        return (acc += points);
      }, 0)
    );
  }

  //Creo variables de bestTeamPoints y worstTeamPoints para almacenar la puntuación max. y min. del array anterior
  let bestTeamPoints = 0;
  let worstTeamPoints = 0;

  //recorro el array arrayTotalScores y adjudico a cada variable el max. o min. valor según corresponda.
  //De esta forma consigo saber la mayor y la menor puntuación recogida en el array arrayTotalScores.
  for (let i = 0; i <= arrayTotalScores.length; i++) {
    if (arrayTotalScores[i] > bestTeamPoints) {
      bestTeamPoints = arrayTotalScores[i];
      worstTeamPoints = bestTeamPoints;
    } else if (arrayTotalScores[i] < worstTeamPoints) {
      worstTeamPoints = arrayTotalScores[i];
    }
  }

  //Creo las variables bestTeamName y worstTeamName buscando el .indexOf del arrayTotalScores.
  //Este índice de la mejor y peor puntuación (en las variables bestTeamPoints y worstTeamPoints)
  //Se corresponde con el mismo índice del mejor y peor equipo en el array inicial firstRound.
  //Por esto accedemos al valor de la propiedad .team utilizando el método .indexOf sobre el
  //array arrayTotalScores.
  const bestTeamName = result[arrayTotalScores.indexOf(bestTeamPoints)].team;
  const worstTeamName = result[arrayTotalScores.indexOf(worstTeamPoints)].team;

  //Solo queda utilizar las 4 variables creadas para hacer un print por consola de los resultados.
  console.log(
    `El mejor equipo es ${bestTeamName} con un total de ${bestTeamPoints} puntos`
  );
  console.log(
    `El peor equipo es ${worstTeamName} con un total de ${worstTeamPoints} puntos`
  );
};

//Llamada de la función que almacena el código pasando como argumento el array inicial firstRound
results(firstRound);
