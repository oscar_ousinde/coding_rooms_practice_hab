/* Crea una funcón que reciba un DNI y lo valide.

El Documento Nacional de Identidad de España (DNI) es el documento de identidad que se expide en España, cada DNI tiene un identificador único compuesto por un número de 8 cifras y una letra, por ejemplo: 99999999-R

¿Sabías que la letra del DNI es un sistema de validación, ya que se obtiene a partir del número mediante un sencillo algoritmo?

Para obtener la letra correspondiente a un número de DNI hay que obtener el resto (en la documentación de JavaScript puedes buscar el operador aritmético para calcular el resto)** **de la división del número entre 23. Ese resto será un número entre 0 y 22. Si usamos ese número como índice en el siguiente Array la letra será la correspondiente a ese índice: 

const letras = [ "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];

Tienes que crear una función en el archivo index.js de la derecha que reciba un DNI completo con este formato: 00000000-T (la letra puede ser mayúscula o minúscula) y debe hacer las siguientes comprobaciones:

Lo que se ha introducido es un String con 10 caracteres (los 8 números, el guión y la letra).

Si separamos el String por el guión tendremos dos partes:

La primera parte (antes del guion) debe tener 8 números.

La segunda parte (después del guion) debe ser un único caracter y no un número.

La letra (segunda parte) debe ser la correcta según el algoritmo explicado anteriormente.

Si se cumplen todas las comprobaciones, se mostrará un mensaje en consola indicando que es un DNI válido.

Si alguna de las comprobaciones falla, lanzará un Error de JavaScript  que diga "Se ha producido un error:" y el mensaje correspondiente. Para esto tienes que usar throw y el objeto Error de JavaScript.
*/

'use strict';

function validateDNI(dni) {
  //Check de caracteres === 10
  if (dni.length !== 10) {
    throw new Error('El DNI introducido no contiene 10 caracteres');
  }

  //Check de solo 8 números en la primera parte. Esta es una forma.
  //   const dniOnlyNumbers = dni.slice(0, 8);
  //   if (dniOnlyNumbers.length !== 8 || isNaN(dniOnlyNumbers)) {
  //     throw new Error('El DNI introducido debe de contener 8 números');
  //   }

  //Check de solo 8 números en la primera parte. Prefiero esta forma para utilizar los template strings.
  const arrayDniNumbers = dni.split('', 8);
  for (const numbers of arrayDniNumbers) {
    if (isNaN(numbers)) {
      throw new Error(
        `El DNI introducido debe de contener 8 números. Has introducido la letra ${numbers} en la posición ${arrayDniNumbers[numbers]}`
      );
    }
  }

  //Comprobación de que la posición después del guión sea una letra
  const afterHyphen = dni.slice(9, 10);
  if (!isNaN(afterHyphen)) {
    throw new Error('La posición 10 del dni debe de ser una letra');
  }

  //COMPROBAR LETRA CORRECTA
  const letras = [
    'T',
    'R',
    'W',
    'A',
    'G',
    'M',
    'Y',
    'F',
    'P',
    'D',
    'X',
    'B',
    'N',
    'J',
    'Z',
    'S',
    'Q',
    'V',
    'H',
    'L',
    'C',
    'K',
    'E',
  ];

  //Declaración de variables para extraer la letra y solo los números del DNI.
  const dniLetter = dni.slice(-1);
  const checkLetter = letras[Number(dni.slice(0, 8) % 23)];
  if (dniLetter !== checkLetter) {
    throw new Error('La letra introducida en el DNI es incorrecta');
  } else {
    console.log('DNI correcto');
  }
}

validateDNI('79341921-V');
// Nota: probaremos con varios números de DNI tanto válidos como no válidos
