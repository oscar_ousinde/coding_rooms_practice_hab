'use strict';
/**
 * Se proporcionan a través del objeto DATE (predifinido en JS). Representa un momento único en el tiempo y es un número
 * llamado Unix epoch. Es el número de segundos desde el 1 enero de 1970 en la hora del meridiano de Greenwicht. La hora UTC.
 * Se representa en número de segundos por ser un número que es fácil de trabajar con el para un amáquina.
 * La fecha en un ordenador representada con Unix epoch es número de segundos desde el 1 enero de 1970 en zona horaria UTC.
 * Las fechas anteriores a esta fecha se calculan con números negativos. La refetencia es siempre el 1 de enero de 1970 a las 00:00.
 * JS utiliza milisegundos.
 */

//Unix epoch en JS

console.log('Unix epoch en este momento:', Date.now()); //nos da e Unix epoch. Date es un constructor con la función now().

//El constructor de fechas Date con fecha actual
//usar con new Date(); nos dará la fecha actual por consola.
const now = new Date();

console.log(now);

//Constructor Date con unix epoch(unix time)

const birthday = new Date(1676447980414);
console.log(birthday);

//Otra forma es pasarle la fecha en texto pero es poco recomendable porque existe variación
console.log(new Date('January 30, 1993'));

//Podemos pasarle una serie de paráemtros y es mas coherente
//arguments: year, month, day, hours, minutes, second,miliseconds
//Los meses para JS empiezan por cero!
console.log(new Date(1987, 0, 3, 16, 35));
console.log(new Date(1987, 11));

//Extraer info de una fecha de la variable anterior now

console.log('Año actual:', now.getFullYear(now));
console.log('Mes actual:', now.getMonth(now));
console.log('Día actual:', now.getDate(now));
console.log('Día de la semanal:', now.getDay(now));

const dayOfWeek = now.getDay();

if (dayOfWeek === 5 || dayOfWeek === 6) {
  console.log('Estamos en fin de semana!!');
} else {
  ('No es fin de semana aún 😑');
}

console.log('Hora actual:', now.getHours());
console.log('Minutos actual:', now.getMinutes());
console.log('Segundos actual:', now.getSeconds());
console.log('Milisegundos actual:', now.getMilliseconds());
console.log('Unix time actual:', now.getTime());
