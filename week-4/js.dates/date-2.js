'use strict';

//Unix epoch/time

//Modificar una fechas

/* const birthday = new Date(1976, 8, 2); */

//Cambiamos el año
/* birthday.setFullYear(1986);
birthday.setMonth(0);
birthday.setDate(15);

console.log(birthday); */

/**
 * Si seteamos por ejemplo .setDate(40), se pasa 10 días de la
 * duración de un mes, por lo que pasará al mes siguiente.
 * Igual con las horas, pasa al día siguiente!
 */

//Añadir 5 días a esta fecha
/* const birthday = new Date(1976, 8, 2);

birthday.setDate(birthday.getDate() + 5);

console.log(birthday); */

//FECHAS EN TEXTO

const now = new Date();

console.log(now.getHours() + ':' + now.getMinutes());
console.log(
  now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear()
);

//Es lo mismo que hacer un console log de now.
//Pero no respeta el formato de fechas del ordenador (el locale).
console.log(now.toString());

//representar fecha con locale
console.log(now.toLocaleString());
console.log(now.toLocaleDateString()); //solo fecha
console.log(now.toLocaleTimeString()); //solo hora

//ISO 8601 para representar fechas de forma inequívoca
//La hora se mostrará simepre en UTC.
console.log(now.toISOString()); //fecha que se entenderá en todo el mundo.
