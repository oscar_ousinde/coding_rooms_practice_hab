const express = require('express');
const cors = require('cors');
const joi = require('joi');
const fs = require('fs/promises');
const path = require('path');

const app = express();
const port = 8080;

app.use(cors());

// Escribe aquí el código solicitado

//parse del body
app.use(express.json());

//GET de /list
app.get('/list', (req, res) => {
  //requerir agenda
  const agenda = require('./data.json');

  //cambiar unix a fechas
  for (const date of agenda) {
    const fecha = date.date;
    date.date = new Date(fecha).toLocaleDateString('es-ES', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
    });
  }

  res.status(200).send({
    status: 'ok',
    message: 'Listado de fechas enviado',
    agenda,
  });
});

//POST de date and event
app.post('/new', (req, res, next) => {
  try {
    //parse a la fecha de la req
    const data = req.body;
    req.body.date = Date.parse(data.date);

    //joi de data
    if (data) {
      const schema = joi.object().keys({
        date: joi.date().timestamp(),
        event: joi.string().min(5).max(20).required(),
      });

      const validation = schema.validate(data);

      if (validation.error) {
        throw validation.error.message;
      }
    }

    //creacion de la agenda
    let agenda;
    agenda = require('./data.json');

    //push a la agenda
    agenda.push(data);

    //función de grabado de la agenda en data.json
    writeDataJson(agenda);

    res.status(200).send({
      status: 'ok',
      message: 'New Date and Event',
    });
  } catch (error) {
    next(error);
  }
});

//función para grabar la agenda en el data.json
const writeDataJson = async (agenda) => {
  try {
    //creacion ruta al data.json
    const agendaPath = path.join(__dirname, 'data.json');

    //editar el fichero
    await fs.writeFile(agendaPath, JSON.stringify(agenda));
  } catch (error) {
    console.log(error.message);
  }
};

app.use((error, req, res, next) => {
  res.send({
    status: 'error',
    message: error.message,
  });
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
