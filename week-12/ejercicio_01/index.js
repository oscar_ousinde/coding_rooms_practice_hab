'use strict';

const getDB = require('./db');

//Escribe a partir de aquí el código solicitado
let connection;

async function createDB() {
  try {
    connection = await getDB();

    //Creación de DB
    await connection.query('CREATE DATABASE IF NOT EXISTS web;');

    //Selección de DB
    await connection.query('USE web;');

    //Eliminación de tablas si existen
    await connection.query('DROP TABLE IF EXISTS likes');
    await connection.query('DROP TABLE IF EXISTS photos');
    await connection.query('DROP TABLE IF EXISTS users');

    //Creación de tablas
    await connection.query(`
    CREATE TABLE users (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR(100),
        name VARCHAR(100),
        registration_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
        active BOOLEAN DEFAULT false
    );
    `);

    await connection.query(`
    CREATE TABLE photos (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        user_id INT UNSIGNED,
        photo_file_name VARCHAR(100),
        creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (user_id) REFERENCES users (id)
    );
    `);

    await connection.query(`
    CREATE TABLE likes (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        user_id INT UNSIGNED,
        photo_id INT UNSIGNED,
        creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (user_id) REFERENCES users (id),
        FOREIGN KEY (photo_id) REFERENCES photos (id)
    );
    `);

    console.log('Se ha creado la DB');
  } catch (error) {
    console.log('Se ha producido un ERROR: ', error.message);
  }
}

createDB();
