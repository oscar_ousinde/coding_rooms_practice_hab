Se proporciona un archivo db.js que exporta una función asíncrona que devuelve un objeto de conexión a la base de datos que tiene un método query()que nos permite ejecutar código SQL, ya conocéis como funciona esto porque lo vimos en clase esta semana.

Con todo esto, edita el fichero index.js, para:

Crear una base de datos que se llame web (solo créala si no existe previamente)

Crear dentro de la base de datos web las siguientes tablascon sus campos y tipos correspondientes usando SQL:

users
    id (number)
    email (text)
    name (text)
    registration_date (datetime)
    active (boolean)

photos
    id (number)
    user_id (number)
    photo_file_name (text)
    creation_date (datetime)

likes
    id (number)
    user_id (number)
    photo_id (number)
    creation_date (datetime)

Establecer las relaciones entre las tablas que consideres necesarias usando SQL.

Antes de crear estas tablas el programa de node debe borrar cualquier otra tabla con el mismo nombre pre-existente en la base de datos.