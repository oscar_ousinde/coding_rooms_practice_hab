'use strict';

const names = [
  'A-Jay',
  'Manuel',
  'Manuel',
  'Eddie',
  'A-Jay',
  'Su',
  'Reean',
  'Manuel',
  'A-Jay',
  'Zacharie',
  'Zacharie',
  'Tyra',
  'Rishi',
  'Arun',
  'Kenton',
];

// Escribe aquí tu código

/* const deleteDuplicates = (arrayWithDuplicates) => {
  const newArray = [...new Set(names)];
  console.log(newArray);
};

deleteDuplicates(names); */

//Otra forma con el bucle forEach y el método includes + push

const eliminateDuplicates = (arrayDuplicates) => {
  const newArray = [];
  arrayDuplicates.forEach((element) => {
    if (!newArray.includes(element)) {
      newArray.push(element);
    }
  });
  return newArray;
};

console.log(eliminateDuplicates(names));
