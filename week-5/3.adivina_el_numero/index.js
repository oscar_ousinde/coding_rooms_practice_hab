'use strict';

function quiz() {
  const randomNum = Math.ceil(Math.random() * 101);
  console.log(randomNum);
  alert(
    `La máquina acaba de generar un número aleatorio entre 0 y 100. ¿Puedes adivinarlo? Tienes 5 intentos`
  );

  for (let i = 5; i >= 0; i--) {
    if (i === 0) {
      alert(`Has perdido. La máquina ha sacado ${randomNum}`);
      break;
    }
    let userNum = Number(
      prompt(`Introduce un número entre 0 y 100. Te quedan ${i} intentos`)
    );
    while (isNaN(userNum) === true || userNum < 0 || userNum > 100) {
      userNum = Number(
        prompt(
          'No has introducido un NÚMERO válido entre 0 y 100, prueba de nuevo'
        )
      );
    }
    if (userNum === randomNum) {
      alert('Has acertado!');
      break;
    } else if (userNum > randomNum) {
      alert(`El número introducido es MAYOR de lo esperado.`);
    } else if (userNum < randomNum) {
      alert(`El número introducido es MENOR de lo esperado.`);
    }
  }
}

quiz();
