'use strict';
const pizzas = [
  'margarita',
  'cuatro quesos',
  'prosciutto',
  'carbonara',
  'barbacoa',
  'tropical',
];

/**
 * En cada iteración tomamos un 'i' que es la mitad de la pizza y el 'j' es la otra mitad. Ponemos una condición.
 * si 'i' es igual a 'j' o 'j' es menor que 'i' --> continue. De esta forma evitamos crear pizzas dobles y no retroceder en
 * el array.
 * Ej:
 * si 'i' es 1 es una mitad de pizza cuatro quesos. Pero 'j' nunca puede ser igual a 1 ni inferior! Dado que en la iteración
 * anterior ya se habrá creado la 'pizza margarita y cuatro quesos'. No queremos una 'cuatro quesos y margarita' porque es lo mismo.
 * Por eso no se crearán las combinaciones de cuatro quesos hasta que 'j' sea 2!. Debe ser mayor que 'i' que en el caso de los
 * cuatro quesos será 1!.
 */

function combine(pizzas) {
  const combinations = [];

  for (let i = 0; i < pizzas.length; i++) {
    for (let j = 0; j < pizzas.length; j++) {
      if (i === j || j < i) {
        continue;
      }
      combinations.push(`${pizzas[i]} y ${pizzas[j]}`);
    }
  }

  return combinations;
}

console.log(combine(pizzas));
