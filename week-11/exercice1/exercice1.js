// 1. Agenda en la línea de comandos

// Crea un programa de node que te permita guardar fechas con descripciones de eventos:

// Utiliza los argumentos del programa para introducir la fecha y el texto de forma que se guarde en un archivo .json.

// Añade también un argumento que imprima los eventos por pantalla.

// Si necesitas usar algún módulo de npm (spoiler: si que necesitas) recuerda que tienes que insalarlo usando la consola que está debajo del editor.

// El programa debe permitir ejecutar un comando como los siguientes para guardar eventos:

// node index.js --date 2022/12/25 --event "Día de Navidad"

// node index.js --data 2023/01/01 --event "Año nuevo!"

// Para simplificar el ejercicio vamos a usar un formato de fecha fácil de manejar en Javascript: YYYY/MM/DD

// El programa también debe permitir listar los eventos guardados que deben aparecer siempre ordenados por fecha (en orden descendiente):

// node index.js --list-events

// Esto debe listar en la consola algo como esto (el icono del calendario es un emoji):

// 📆 2023/01/01: Año nuevo

// 📆 2022/12/25: Día de Navidad

const minimist = require("minimist");
const fs = require("fs/promises");
const path = require("path");

//Procesado de argumentos
const args = minimist(process.argv.slice(2));
const { date, event } = args;
console.log(date, event);

//Creando el json
const dateEvent = {
  date: `${date}`,
  event: `${event}`,
};

const pathJson = path.resolve(__dirname, "calendar.json");

async function createJsonIfNotExists(pathJson, dateEvent) {
  try {
  } catch {
    const dateEventJson = JSON.stringify(dateEvent);
    await fs.writeFile(pathJson, dateEventJson, "utf-8");
  }
}

createJsonIfNotExists(pathJson, dateEvent);
