// Modifica el servidor web del editor de la derecha para que exponga:

// Una ruta que devuelva la hora actual.

// Una ruta que devuelva el directorio del archivo dónde se haya el código del servidor.

// Un middleware que imprima por consola información de cada request:

// la URL de la request

// el método de la request

// Un middleware que gestione los errores y otro que se ocupe de los errores 404.

// Lanza el servidor web pulsando el botón "Run" de color verde de la parte superior del editor y en la parte de abajo verás la url que podrás copiar y pegar en otro tab del navegador para hacer las comprobaciones de que todo funciona.

// Para probar el middleware de error puedes acceder a la ruta /error-forzado que ya está creada y que genera un error.

//  Para probar el error 404 simplemente accede a una ruta no definida.

const express = require("express");
const app = express();
const port = 8080;

const fs = require("fs");
const path = require("path");

// Escribe aquí el código solicitado

app.use((req, res, next) => {
  console.log(`El método es: ${req.method}`);
  console.log(`La petición es http://localhost:8080${req.url}`);
  next();
});
// Una ruta que devuelva la hora actual.
app.get("/hour", (req, res) => {
  const hour = new Date().getHours().toString().padStart(2, 0);
  const minutes = new Date().getMinutes().toString().padStart(2, 0);

  const hourMinutes = hour + ":" + minutes;

  res.send(hourMinutes.toString());
});

// Una ruta que devuelva el directorio del archivo dónde se haya el código del servidor.
app.get("/directory", (req, res) => {
  const pathDirectory = path.resolve(__dirname);
  res.send(pathDirectory);
});

// Un middleware que imprima por consola información de cada request:

// la URL de la request

// el método de la request

// Un middleware que gestione los errores y otro que se ocupe de los errores 404.
//middleware de los errores
//accediendo a esta ruta se genera el error que debe recogerse en el middleware de los errores
app.get("/error-forzado", (req, res, next) => {
  return next(new Error("Este es un error generado intencionadamente"));
});

app.use((error, req, res, next) => {
  error.status = 404;
  res.status(error.status).send({
    status: "error",
    message: "Middleware de los errores",
  });
});

//errores 404
app.use((req, res) => {
  res.status(404).send({
    status: "error",
    message: "Not found. Error 404",
  });
});

//escucha del servidor
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
