// Crea un programa que recoja a través de sus argumentos la ruta de una imagen de forma que cree en el mismo directorio que esta una carpeta samples con diferentes variaciones de la misma:x

// miniatura de 200px de ancho

// tamaño medio de 500px de ancho

// tamaño medio del tamaño anterior pero en blanco y negro

// La imagen que tienes que procesar ya está subida y está en el mismo directorio que el archivo index.js, se llama cat.jpg. Pero debería funcionar con cualquier otra imagen subida. El comando a usar sería:

// node index.js cat.jpg

// Usa la librería sharp (https://www.npmjs.com/package/sharp) que vimos en el vídeo semanal.

// La librería sharp ya está en el package.json por lo que es necesario hacer npm install antes de ejecutar el programa para instalar la dependencia.

// Escribe tu código a continuación

const path = require("path");
const fs = require("fs/promises");
const sharp = require("sharp");

//Procesar argumentos
const image = process.argv.slice(2)[0];

const imagePath = path.resolve(__dirname, image);

//Procesar la imagen
const samples = path.resolve(__dirname, "samples");

//Función crear directorio samples si no existe
async function createPathIfNotExists(path) {
  try {
    await fs.access(path);
  } catch {
    await fs.mkdir(path);
  }
}

createPathIfNotExists(samples);

//Función procesar imagen y guardarla en el directorio samples
async function processImage(imagePath, samples, image) {
  try {
    const imageProcess = sharp(imagePath);
    imageProcess.resize(200);
    await imageProcess.toFile(path.resolve(samples, `200px_${image}`));
    imageProcess.resize(500);
    await imageProcess.toFile(path.resolve(samples, `500px_${image}`));
    imageProcess.greyscale();
    await imageProcess.toFile(path.resolve(samples, `b&w_${image}`));
  } catch (err) {
    console.error("La ruta no existe");
  }
}

processImage(imagePath, samples, image);
