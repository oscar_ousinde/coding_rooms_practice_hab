-- EJERCICIO 1

-- Escribe en el editor de la derecha el SQL necesario para crear una tabla llamada students dentro de la base de datos bootcamp que creamos y activcamos en las líneas 1 y 2.

-- Esta tabla debe permitir guardar los siguiente campos:

-- ID numérica, única y que se incremente automáticamente al introducir nuevas filas.

-- Nombre del estudiante

-- Apellidos del estudiante

-- Email del estudiante

-- Teléfono del estudiante

-- DNI del estudiante

-- País del Estudiante

-- Código Postal del estudiante

-- Dirección del estudiante

-- Ciudad del Estudiante 

-- Configura los campos según creas que es necesario para cada tipo de datos.

CREATE DATABASE IF NOT EXISTS bootcamp;

USE bootcamp;

CREATE TABLE IF NOT EXISTS students(
	id INT UNIQUE AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    surname VARCHAR(50),
    email VARCHAR(255),
    phone INT,
    dni INT,
    country VARCHAR(50),
    postcode INT,
    address VARCHAR(255),
    city VARCHAR(50)
);

-- EJERCICIO 2
-- Crea una nueva tabla addresses con los campos de dirección necesarios.
-- Modifica mediante la sentencia SQL ALTER TABLE la tabla students que creaste en el ejercicio anterior para eliminar los campos de dirección y crea los campos necesarios para que las dos tablas estén relacionadas mediante una FOREIGN KEY.

DROP TABLE addresses;
CREATE TABLE IF NOT EXISTS addresses(
	id INT UNIQUE NOT NULL PRIMARY KEY AUTO_INCREMENT,
	address VARCHAR(255),
	city VARCHAR(50),
	postcode INT,
	country VARCHAR(50),
    FOREIGN KEY (id) REFERENCES students(id)
);

ALTER TABLE students
DROP COLUMN country,
DROP COLUMN postcode,
DROP COLUMN address,
DROP COLUMN city;

-- EJERCICIO 3
-- id,first_name,last_name,email,tlf,DNI,country,cp,address,city
-- ------------------------------------------------------
-- 1,Irvin,Lethem,ilethem0@google.com.au,993870144,279948941-9,Indonesia,83297,98339 Loftsgordon Road,Babakanbandung
-- 2,Kylie,Mungan,kmungan1@howstuffworks.com,497494899,748551874-7,Philippines,44455,74641 Dwight Avenue,Bilar
-- 3,Yul,Dibbert,ydibbert2@businesswire.com,776631050,215649413-4,Indonesia,62965,9510 Milwaukee Street,Sumberejo
-- 4,Tamra,Mc Gorley,tmcgorley3@studiopress.com,921948685,617064473-7,Norway,54756,8902 Doe Crossing Alley,Steinkjer
-- 5,Elmira,Imbrey,eimbrey4@cpanel.net,304168000,178988896-4,United States,51471,8616 Stephen Hill,Charleston

INSERT INTO students(name, surname, email, phone, dni)
	VALUES ('Irvin', 'Lethem', 'ilethem0@google.com.au', 993870144, 279948941-9),
		   ('Kylie', 'Mungan', 'kmungan1@howstuffworks.com', 497494899, 748551874-7),
           ('Yul', 'Dibbert', 'ydibbert2@businesswire.com',776631050, 215649413-4),
           ('Tamra', 'Mc Gorley', 'tmcgorley3@studiopress.com', 921948685, 617064473-7),
           ('Elmira', 'Imbrey', 'eimbrey4@cpanel.ne', 304168000, 178988896-4);
           
INSERT INTO addresses(address, city, postcode, country)
	VALUES ('Loftsgordon Road', 'Babakanbandung', 98339, 'Indonesia'),
		   ('Dwight Avenue', 'Bilar', 74641, 'Philippines'),
           ('Milwaukee Street', 'Sumberejo', 9510, 'Indonesia'),
           ('Doe Crossing Alley', 'Steinkjer', 8902, 'Norway'),
           ('Stephen Hill', 'Charleston', 8616, 'United States');
           
-- EJERCICIO 4

-- seleccionar name, surname y phone y ordenar por surname
SELECT name, surname, phone
FROM students
ORDER BY surname;

-- usuarios de cada país basado en la tabla direcciones
SELECT COUNT(id) AS studentsPerCountry, country
FROM addresses
GROUP BY country;
