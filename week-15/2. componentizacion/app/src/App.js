import './App.css';
import ImagePost from './components/ImagePost';
import Publisher from './components/Publisher';
import PostText from './components/PostText';
import UsersComments from './components/UsersComments';
import LikeFavCommentShare from './components/LikeFavCommentShare';
import TotalLikes from './components/TotalLikes';
import AddComment from './components/AddComment';

function App() {
  return (
    <main>
      <section>
        <article>
          <ImagePost />
        </article>
        <article>
          <Publisher accountName='javascript.tips' />
          <PostText accountName='javascript.tips' />
          <UsersComments accountName='javascript.tips' />
          <LikeFavCommentShare />
          <TotalLikes />
          <AddComment />
        </article>
      </section>
    </main>
  );
}

export default App;
