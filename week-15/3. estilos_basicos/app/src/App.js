import './App.css';

function App() {
  return (
    <main>
      <h1 style={{ backgroundColor: `rgb(${red}, ${blue}, ${green})` }}>
        h1 que cambia de color
      </h1>
      <p className='importante'>Un p importante</p>
    </main>
  );
}

const red = Math.round(Math.random() * 255);
const blue = Math.round(Math.random() * 255);
const green = Math.round(Math.random() * 255);

export default App;
