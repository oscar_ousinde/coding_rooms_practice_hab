import './App.css';

function App() {
  return (
    <main>
      <p>Variable de entorno: {process.env.REACT_APP_TEXT}</p>
      {/* <img src={process.env.PUBLIC_URL + 'foto.jpg'} alt='cat programming' /> */}
      <img src='/foto.jpg' alt='cat programming' />
    </main>
  );
}

export default App;
